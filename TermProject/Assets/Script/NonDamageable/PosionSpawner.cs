﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosionSpawner : NonDamageableObject
{
    float arriveTime;
    int posionType;

    void Start()
    {
        arriveTime = 0;
        posionType = Random.Range(0, 2);
    }

    // Update is called once per frame
    void Update()
    {
        arriveTime += Time.deltaTime;
        if (arriveTime > 2)
        {
            MonsterManager.instance.HatchPosion(gameObject, posionType);
        }
    }
}
