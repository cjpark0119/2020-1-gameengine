﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : NonDamageableObject
{
    float arriveTime;
    int monsterType;

    void Start()
    {
        arriveTime = 0;
        monsterType = Random.Range(0, 2);
    }

    // Update is called once per frame
    void Update()
    {
        arriveTime += Time.deltaTime;
        if(arriveTime > 5)
        {
            MonsterManager.instance.HatchMonster(gameObject, monsterType);
        }
    }
}
