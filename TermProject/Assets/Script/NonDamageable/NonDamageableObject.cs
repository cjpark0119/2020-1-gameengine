﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NonDamageableObject : RootObject
{

    protected virtual void Awake()
    {

    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }
}
