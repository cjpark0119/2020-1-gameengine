﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Posion : NonDamageableObject
{
    public int posion_type;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject);
        if (collision.gameObject.tag == "Player")
        {
            SoundManager.instance.PlaySingle(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().addPosionSound);
            if (posion_type == CustomVariables.POSION_HP)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().GetHp(30);
            }
            else{
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().GetStemina(40);
            }
            Destroy(gameObject);
        }
    }
}
