﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour
{
    public Color startColor;
    public Color endColor;
    float CycleTime;
    float lerp;
    bool IsIncrease;

    void Start()
    {
        CycleTime = 60;
        lerp = 0;
        IsIncrease = true;
        GetComponent<Light>().color = startColor;
    }

    void Update()
    {
        float elapsedTime = Time.deltaTime;
        if (IsIncrease)
        {
            lerp += elapsedTime;
            if(lerp > CycleTime)
            {
                lerp = CycleTime - (lerp - CycleTime);
                IsIncrease = !IsIncrease;
            }
        }
        else
        {
            lerp -= elapsedTime;
            if (lerp < 0)
            {
                lerp = -lerp;
                IsIncrease = !IsIncrease;
            }
        }
        GetComponent<Light>().color = Color.Lerp(startColor, endColor, lerp / CycleTime);
        //Debug.Log(RenderSettings.skybox.GetFloat("_Exposure"));
        RenderSettings.skybox.SetFloat("_Exposure", (1.2f - (lerp / CycleTime) * 1.2f) + 0.3f);
    }
}
