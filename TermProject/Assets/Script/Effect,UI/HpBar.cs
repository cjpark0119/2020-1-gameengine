﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBar : MonoBehaviour
{
    RectTransform curHPbar;
    float maxBarLenght;

    private void Start()
    {
        curHPbar = transform.GetChild(0).GetComponent<RectTransform>();
        maxBarLenght = transform.GetComponent<RectTransform>().rect.width;
    }

    public void SetBar(int cur_hp, int max_hp)
    {
        float interpolate = cur_hp / (float)max_hp;
        Vector3 cur_pos = curHPbar.localPosition;
        curHPbar.localPosition = new Vector3(-(1- interpolate) * maxBarLenght * 0.5f, cur_pos.y, cur_pos.z);
        curHPbar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, interpolate * maxBarLenght);  // width 수정
    }
}
