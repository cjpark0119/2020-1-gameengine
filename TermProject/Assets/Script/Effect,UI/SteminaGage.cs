﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SteminaGage : MonoBehaviour
{
    private RectTransform gage;
    private Image gageBorder;
    private float gageRate;
    private float height;
    private Vector3 gageStartPos;

    private void Awake()
    {
        gage = transform.GetChild(0).GetComponent<RectTransform>();
        gageBorder = transform.GetChild(1).GetComponent<Image>();
        height = GetComponent<RectTransform>().sizeDelta.y;
        gageStartPos = gage.localPosition - Vector3.up * height;
    }

    void Start()
    {
        gageRate = GameManager.instance.player.stemina;
        gage.localPosition = Vector3.Lerp(gageStartPos, gageStartPos + Vector3.up * height, gageRate / 100);
    }

    // Update is called once per frame
    void Update()
    {
        gageRate = GameManager.instance.player.stemina;
        gage.localPosition = Vector3.Lerp(gageStartPos, gageStartPos + Vector3.up * height, gageRate / 100);
    }
}
