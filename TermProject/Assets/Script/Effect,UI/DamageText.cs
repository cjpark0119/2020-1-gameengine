﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageText : MonoBehaviour
{
    void Start()
    {
        transform.rotation = Camera.main.transform.localRotation;
        transform.position += transform.up * 0.7f;
    }

    public void SetDamageText(int amount, bool isHeal)
    {
        if (isHeal)
        {
            transform.GetComponent<TextMesh>().color = Color.blue;
            transform.GetComponent<TextMesh>().text = "+" + amount;
        }
        else
        {
            transform.GetComponent<TextMesh>().color = Color.red;
            transform.GetComponent<TextMesh>().text = "-" + amount;
        }
        StartCoroutine(Move());
    }

    public IEnumerator Move()
    {
        float distance = 0;
        while (true)
        {
            if (distance > 1)
                break;

            float tmp = 1.5f * Time.deltaTime;
            distance += tmp;
            transform.Translate(transform.up * tmp, Space.World);
            transform.rotation = Camera.main.transform.localRotation;
            yield return null;
        }
        Destroy(gameObject);
    }
}
