﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] GameObject target;
    Transform target_transform;
    [SerializeField] Vector3 offsetToTarget;

    void Start()
    {
        target_transform = target.transform;
        transform.position = target_transform.position + (target_transform.forward * offsetToTarget.z) + (new Vector3(0, 1.2f, 0) * offsetToTarget.y);
        transform.forward = Vector3.Normalize((target_transform.position + new Vector3(0,1,0) * 1.7f) - transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll > 0)
        {
            if (offsetToTarget.magnitude > 2.5f)
                offsetToTarget -= Vector3.Normalize(offsetToTarget) * scroll;
        }
        else if(scroll < 0)
        {
            if (offsetToTarget.magnitude < 7)
                offsetToTarget -= Vector3.Normalize(offsetToTarget) * scroll;
        }

        if (Input.GetMouseButton(0))
        {
            
        }
        transform.position = target_transform.position + (target_transform.forward * offsetToTarget.z) + (new Vector3(0, 1.2f, 0) * offsetToTarget.y);
        transform.forward = Vector3.Normalize((target_transform.position + new Vector3(0, 1, 0) * 1.7f) - transform.position);
    }
}
