﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : DamageableObject
{

    protected override void Awake()
    {
        max_hp = CustomVariables.HP_GATE;
        hp = max_hp;
    }

    void Start()
    {
        UIManager.instance.SetGateStateText(hp, max_hp);
    }

    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "FireBall")
        {
            EffectManager.instance.OnEffect(CustomVariables.EFT_MONSTER_ATTACK, collision.gameObject.transform.position + transform.up, transform.forward);
            GetComponent<DamageableObject>().TakeDamage(7);
            Destroy(collision.gameObject);
        }
    }
}
