﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    int damage { get; set; }
    int max_hp { get; set; }
    int hp { get; set; }
    bool TakeDamage(int damage);    // 명시만 할 수 있다.
}
