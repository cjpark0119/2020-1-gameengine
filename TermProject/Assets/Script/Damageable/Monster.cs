﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : DamageableObject
{
    protected GameObject gate;
    protected GameObject player;
    protected Transform g_transform;
    protected Transform p_transform;
    protected Animator animator;
    public bool isTargetGate { get; set; }
    protected float attack_delay;
    protected int cur_state;
    protected bool isAttack;
    protected float useAttackTime; // 공격시전 시간

    protected override void Awake()
    {
        isTargetGate = true;
    }

    protected virtual void Start()
    {
        gate = GameObject.FindGameObjectWithTag("Gate");
        player = GameObject.FindGameObjectWithTag("Player");
        g_transform = gate.transform;
        p_transform = player.transform;
        HpBarManager.instance.UpdateHpBar(this);
        cur_state = CustomVariables.STATE_MOVE;
        animator = GetComponent<Animator>();
    }

    public void TargetIsPlayer()
    {
        isTargetGate = false;
    }

    public void TargetIsGate()
    {
        isTargetGate = true;
    }

    public virtual void Attack()
    {

    }

    protected void MoveToTarget()
    {
        if (cur_state == CustomVariables.STATE_ATTACK)
        {
            if(o_type == CustomVariables.WOLF_ORC)
                animator.SetTrigger("AttackToRun");
            else
                animator.SetTrigger("AttackToWalk");
            cur_state = CustomVariables.STATE_MOVE;
        }

        Vector3 targetPos = isTargetGate ? g_transform.position : p_transform.position;
        Vector3 toTarget = Vector3.Normalize(targetPos - transform.position);

        float DotResult = Vector3.Dot(transform.forward, toTarget);
        if (DotResult > 0)   // 가고자 하는 방향이 시야 안이라면
        {
            if (DotResult > (1f - 0.00001f))
            {
                transform.position += toTarget * Time.deltaTime * speed;
            }
            else
            {
                if (Vector3.Cross(transform.forward, toTarget).y > 0)  // 회전방향이 오른쪽이라면
                {
                    transform.Rotate(new Vector3(0, 1, 0), rotate_speed * Time.deltaTime);
                    if (Vector3.Cross(transform.forward, toTarget).y < 0)
                        transform.LookAt(transform.position + toTarget, new Vector3(0, 1, 0));
                }
                else
                {
                    transform.Rotate(new Vector3(0, 1, 0), -rotate_speed * Time.deltaTime);
                    if (Vector3.Cross(transform.forward, toTarget).y > 0)
                        transform.LookAt(transform.position + toTarget, new Vector3(0, 1, 0));
                }

                transform.position += transform.forward * Time.deltaTime * speed * 0.3f;
            }
        }
        else
        {
            if (Vector3.Cross(transform.forward, toTarget).y > 0)  // 회전방향이 오른쪽이라면
            {
                transform.Rotate(0, rotate_speed * Time.deltaTime, 0, Space.World);
            }
            else
            {
                transform.Rotate(0, -rotate_speed * Time.deltaTime, 0, Space.World);
            }
        }
    }
}
