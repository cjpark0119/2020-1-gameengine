﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShamanOrc : Monster
{
    bool isSpell;
    [SerializeField] GameObject ballPrefab;

    protected override void Awake()
    {
        base.Awake();
        max_hp = CustomVariables.HP_SHAMAN_ORC;
        hp = max_hp;
        damage = CustomVariables.DMG_SHAMAN_ORC;
        o_type = CustomVariables.SHAMON_ORC;
        speed = CustomVariables.SPEED_SHAMAN_ORC;
        rotate_speed = 120f;
        attack_delay = 0;
        useAttackTime = CustomVariables.ATK_SPEED_SHAMAN_ORC;
    }

    protected override void Start()
    {
        base.Start();
    }

    void FireShamanBall()
    {
        ShamanBall shamanBall = Instantiate(ballPrefab, transform.position + new Vector3(0, 0.7f, 0) + transform.forward * 0.5f, Quaternion.identity, transform).GetComponent<ShamanBall>();
        shamanBall.dir = isTargetGate ? Vector3.Normalize((gate.transform.position + new Vector3(0, 0.5f, 0)) - shamanBall.gameObject.transform.position) :
            Vector3.Normalize((player.transform.position + new Vector3(0, 0.5f, 0)) - shamanBall.gameObject.transform.position);
    }

    public override void Attack()
    {
        if (cur_state == CustomVariables.STATE_MOVE)    // 처음 들어왔다면
        {
            useAttackTime = 0;
            attack_delay = 0;
            cur_state = CustomVariables.STATE_ATTACK;
            isSpell = false;
            isAttack = false;
        }
        attack_delay += Time.deltaTime;

        if (isSpell == false && attack_delay > 0.4f)    // 0.4초 후에 주문 시전
        {
            animator.SetTrigger("WalkToSpell");
            isSpell = true;
        }

        if(isAttack == false && attack_delay > 1.5f)  // 1.5초 후에 공격
        {
            FireShamanBall();
            isAttack = true;
        }
    }

    void Update()
    {
        if (GameManager.instance.running_game == true)
        {
            useAttackTime += Time.deltaTime;
            if (useAttackTime > 2 && cur_state == CustomVariables.STATE_ATTACK)
            {
                cur_state = CustomVariables.STATE_MOVE;
                animator.SetTrigger("SpellToWalk");
            }

            Vector3 targetPos = isTargetGate ? g_transform.position : p_transform.position;

            if (cur_state == CustomVariables.STATE_ATTACK && attack_delay < 2)   // 현재 공격상태에 들어가 있다.
            {
                Attack();
            }
            else
            {
                if (isTargetGate)   // 타겟이 성문이였지만 플레이어가 공격범위 안에 들어오면 플레이어를 타겟으로 바꾼다.
                    if (Vector3.SqrMagnitude(p_transform.position - transform.position) < (CustomVariables.RNG_SHAMAN_ORC * CustomVariables.RNG_SHAMAN_ORC))
                    {
                        isTargetGate = false;
                        targetPos = p_transform.position;
                    }

                if (Vector3.SqrMagnitude(targetPos - transform.position) < (CustomVariables.RNG_SHAMAN_ORC * CustomVariables.RNG_SHAMAN_ORC))  // 타겟이 시야에 있다면,
                {
                    if (useAttackTime > CustomVariables.ATK_SPEED_SHAMAN_ORC) // 시야에 있고 공격 쿨타임이 돌아왔다면
                    {
                        if (isTargetGate)
                            Attack();
                        else
                        {
                            if (Vector3.Dot(Vector3.Normalize(targetPos - transform.position), transform.forward) > 0.8f)
                                Attack();
                            else
                                MoveToTarget();
                        }
                    }
                }
                else
                    MoveToTarget();
            }
        }
    }
}
