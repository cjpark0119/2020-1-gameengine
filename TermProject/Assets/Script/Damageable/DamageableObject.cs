﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

// IDamageable 인터페이스를 사용하여 hp를 관리
public abstract class DamageableObject : RootObject, IDamageable
{
    public int max_hp { get; set; }
    public int hp { get; set; }
    public float speed { get; set; }
    public int damage { get; set; }
    protected float rotate_speed;
    protected int o_type;

    protected virtual void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }

    public bool TakeDamage(int damage)
    {
        hp -= damage;
        EffectManager.instance.OnEffect(CustomVariables.EFT_DAMAGE_TEXT, transform.position, transform.forward, damage);

        if (tag == "Monster")    // 데미지를 입었을 때 몬스터 타입에 따른 작업
        {
            if (hp < 1)
            {
                MonsterManager.instance.DeleteMonster(GetComponent<Monster>());
                return true;
            }
            else
                HpBarManager.instance.UpdateHpBar(GetComponent<Monster>());
        }
        else if (tag == "Gate")    
        {
            if (hp < 1)     // 성문의 피가 0으로 내려가면 게임 종료하고 로비씬으로 전환
            {
                StartCoroutine(DeathAction());
                SoundManager.instance.PlaySingle(SoundManager.instance.dieSound);
                return true;
            }
            else
                HpBarManager.instance.UpdateGateHpBar();
        }
        else if (tag == "Player")
        {
            SoundManager.instance.PlaySingle(SoundManager.instance.hitSound);
            if (hp < 1)     // 플레이어의 피가 0으로 내려가면 게임 종료하고 로비씬으로 전환
            {
                StartCoroutine(DeathAction());
                SoundManager.instance.PlaySingle(SoundManager.instance.dieSound);
                return true;
            }
            else
                UIManager.instance.SetPlayerStateText(hp, max_hp);
        }
        return false;
    }

    public IEnumerator DeathAction()
    {
        GameManager.instance.running_game = false;
        GameManager.instance.gameObject.SetActive(false);
        float age = 0;
        bool death_action = false;
        while (true)
        {
            if(age > 0.2f && death_action == false)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().Play("Armature|Death");
                death_action = true;
            }
            if (age > 2.5f)
                break;

            age += Time.deltaTime;
            yield return null;
        }
        HpBarManager.instance.DeleteAllHpBar();
        GameObject gameoverFrame = UIManager.instance.GetGameOverFrame().gameObject;
        gameoverFrame.transform.GetChild(1).GetComponent<Text>().text = "Score : " + GameManager.instance.GetScore();
        gameoverFrame.SetActive(true);
    }

    public bool AttackTarget(DamageableObject target)
    {
        return target.TakeDamage(damage);
    }

    public void Move(Vector3 shift)
    {
        float DotResult = Vector3.Dot(transform.forward, shift);
        if (DotResult > 0)   // 가고자 하는 방향이 시야 안이라면
        {
            if (DotResult > (1f - 0.00001f))
            {
                transform.position += shift * Time.deltaTime * speed;
            }
            else
            {
                if (Vector3.Cross(transform.forward, shift).y > 0)  // 회전방향이 오른쪽이라면
                {
                    transform.Rotate(new Vector3(0, 1, 0), rotate_speed * Time.deltaTime);
                    if (Vector3.Cross(transform.forward, shift).y < 0)
                        transform.LookAt(transform.position + shift, new Vector3(0, 1, 0));
                }
                else
                {
                    transform.Rotate(new Vector3(0, 1, 0), -rotate_speed * Time.deltaTime);
                    if (Vector3.Cross(transform.forward, shift).y > 0)
                        transform.LookAt(transform.position + shift, new Vector3(0, 1, 0));
                }

                transform.position += transform.forward * Time.deltaTime * speed * 0.3f;
            }
        }
        else
        {
            if (Vector3.Cross(transform.forward, shift).y > 0)  // 회전방향이 오른쪽이라면
            {
                transform.Rotate(0, rotate_speed * Time.deltaTime, 0, Space.World);
            }
            else
            {
                transform.Rotate(0, -rotate_speed * Time.deltaTime, 0, Space.World);
            }
        }
    }
}
