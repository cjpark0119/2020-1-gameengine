﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum STATE { NONE, IDLE_STATE, RUN_STATE, ATTACK_STATE, HIT_STATE };

static public class IdleState
{
    static public void Enter(Player player)
    {
        switch (player.prev_state)
        {
            case STATE.RUN_STATE:
                player.animator.SetTrigger("RunToIdle");
                break;
            case STATE.ATTACK_STATE:
                {
                    if (player.attackType == 0)
                        player.animator.SetTrigger("Attack1ToIdle");
                    else if (player.attackType == 1)
                        player.animator.SetTrigger("Attack2ToIdle");
                    else if (player.attackType == 2)
                        player.animator.SetTrigger("PowerAttackToIdle");
                }
                break;
        }
    }

    static public void Update(Player player)
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.GetKey(KeyCode.LeftControl) && player.stemina > 30) 
            {
                player.stemina -= 30;
                player.attackType = 2;  // 강공격
            }
            else
                player.attackType = Random.Range(0, 2); // 약공격
            player.next_state = STATE.ATTACK_STATE;
        }
        else if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            player.next_state = STATE.RUN_STATE;
    }

    static public void Exit(Player player)
    {

    }
}


static public class RunState
{
    static public void Enter(Player player)
    {
        switch (player.prev_state)
        {
            case STATE.IDLE_STATE:
                player.animator.SetTrigger("IdleToRun");
                break;
            case STATE.ATTACK_STATE:
                {
                    if (player.attackType == 0)
                        player.animator.SetTrigger("Attack1ToRun");
                    else if (player.attackType == 1)
                        player.animator.SetTrigger("Attack2ToRun");
                    else if (player.attackType == 2)
                        player.animator.SetTrigger("PowerAttackToRun");
                }
                break;
        }
    }

    static public void Update(Player player)
    {

        Vector3 shift = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.A))
        {
            Vector3 cameraLeft = -player.camera.transform.right;
            cameraLeft.y = 0;
            cameraLeft = Vector3.Normalize(cameraLeft);
            shift += cameraLeft;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Vector3 cameraRight = player.camera.transform.right;
            cameraRight.y = 0;
            cameraRight = Vector3.Normalize(cameraRight);
            shift += cameraRight;
        }
        if (Input.GetKey(KeyCode.W))
        {
            Vector3 cameraLook = player.camera.transform.forward;
            cameraLook.y = 0;
            cameraLook = Vector3.Normalize(cameraLook);
            shift += cameraLook;
        }

        if (Input.GetKey(KeyCode.LeftShift) && player.stemina > 10 * Time.deltaTime)
        {
            player.stemina -= 6 * Time.deltaTime;
            player.speed = CustomVariables.SPEED_PLAYER_MAX;
        }
        else
            player.speed = CustomVariables.SPEED_PLAYER_BASE;


        if (Input.GetMouseButtonDown(0))
        {
            if (Input.GetKey(KeyCode.LeftControl) && player.stemina > 30)
            {
                player.stemina -= 30;
                player.attackType = 2;  // 강공격
            }
            else
                player.attackType = Random.Range(0, 2);     // 약공격
            player.next_state = STATE.ATTACK_STATE;
        }
        else if (shift != Vector3.zero)
            player.Move(Vector3.Normalize(shift));
        else
            player.next_state = STATE.IDLE_STATE;
    }

    static public void Exit(Player player)
    {

    }
}



static public class AttackState
{
    static float enterTime;
    static bool onEffect; 

    static public void Enter(Player player)
    {
        enterTime = 0f;
        onEffect = false;
        switch (player.prev_state)
        {
            case STATE.IDLE_STATE:
                {
                    if (player.attackType == 0)
                        player.animator.SetTrigger("IdleToAttack1");
                    else if (player.attackType == 1)
                        player.animator.SetTrigger("IdleToAttack2");
                    else if (player.attackType == 2)
                        player.animator.SetTrigger("IdleToPowerAttack");
                }
                break;
            case STATE.RUN_STATE:
                {
                    if (player.attackType == 0)
                        player.animator.SetTrigger("RunToAttack1");
                    else if (player.attackType == 1)
                        player.animator.SetTrigger("RunToAttack2");
                    else if (player.attackType == 2)
                        player.animator.SetTrigger("RunToPowerAttack");
                }
                break;
        }

    }

    static public void Update(Player player)
    {
        enterTime += Time.deltaTime;
        if(player.attackType == 2)  // 강공격이라면
        {
            if(!onEffect && enterTime > 0.7)
            {
                SoundManager.instance.PlaySingle(player.attackSound);
                player.AttackMonsters(); // 몬스터 다수 공격 함수
                onEffect = true;
            }
            if (enterTime > 0.9f)
                player.next_state = STATE.IDLE_STATE;
        }
        else
        {
            if (!onEffect && enterTime > 0.4)
            {
                SoundManager.instance.PlaySingle(player.attackSound);
                player.AttackMonster(); // 몬스터 한 마리 공격 함수
                onEffect = true;
            }
            if (enterTime > 0.5f)
                player.next_state = STATE.IDLE_STATE;
        }
        
    }

    static public void Exit(Player player)
    {

    }
}



static public class HitState
{
    static public void Enter(Player player)
    {
        
    }
    static public void Update(Player player)
    {

    }

    static public void Exit(Player player)
    {

    }
}


public class Player : DamageableObject
{
    public Camera camera = null;
    public Animator animator;
    public STATE cur_state;
    public STATE next_state;
    public STATE prev_state;
    public int attackType;
    public float stemina;
    private float gageIncreaseSpeed;
    public AudioClip attackSound;
    public AudioClip addPosionSound;

    protected override void Awake()
    {
        max_hp = CustomVariables.HP_PLAYER;
        hp = max_hp;
        damage = CustomVariables.DMG_PLAYER;
        stemina = CustomVariables.STEMINA_PLAYER * 0.7f;
        speed = CustomVariables.SPEED_PLAYER_BASE;
        gageIncreaseSpeed = 3f;
        rotate_speed = 180f;
        camera = Camera.main;
        animator = GetComponent<Animator>();
        o_type = CustomVariables.PLAYER;
    }

    void Start()
    {
        cur_state = STATE.IDLE_STATE;
        next_state = STATE.NONE;
        prev_state = STATE.IDLE_STATE;

        UIManager.instance.SetPlayerStateText(hp, max_hp);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.running_game == true)
        {
            stemina += Time.deltaTime * gageIncreaseSpeed;
            if (stemina > 100)
                stemina = 100;

            if (next_state != STATE.NONE)
                ExchangeState();

            switch (cur_state)
            {
                case STATE.IDLE_STATE:
                    IdleState.Update(this);
                    break;
                case STATE.RUN_STATE:
                    RunState.Update(this);
                    break;
                case STATE.ATTACK_STATE:
                    AttackState.Update(this);
                    break;
                case STATE.HIT_STATE:
                    HitState.Update(this);
                    break;
            }

        }
    }

    void ExchangeState()
    {
        switch (cur_state)
        {
            case STATE.IDLE_STATE:
                IdleState.Exit(this);
                break;
            case STATE.RUN_STATE:
                RunState.Exit(this);
                break;
            case STATE.ATTACK_STATE:
                AttackState.Exit(this);
                break;
            case STATE.HIT_STATE:
                HitState.Exit(this);
                break;
            default:
                Debug.Log("State is Error!");
                break;
        }
        prev_state = cur_state;
        cur_state = next_state;

        switch (cur_state)
        {
            case STATE.IDLE_STATE:
                IdleState.Enter(this);
                break;
            case STATE.RUN_STATE:
                RunState.Enter(this);
                break;
            case STATE.ATTACK_STATE:
                AttackState.Enter(this);
                break;
            case STATE.HIT_STATE:
                HitState.Enter(this);
                break;
            default:
                Debug.Log("State is Error!");
                break;
        }

        next_state = STATE.NONE;
    }

    public void AttackMonsters()    // 다중 공격
    {
        List<Monster> monsters = MonsterManager.instance.monsters;
        int monsterCount = monsters.Count;

        for (int i = 0; i < monsterCount; ++i)
        {
            if (Vector3.SqrMagnitude(transform.position - monsters[i].transform.position) < 
                (CustomVariables.RNG_PLAYER_POWER_ATTACK * CustomVariables.RNG_PLAYER_POWER_ATTACK))    // 적이 플레이어의 일정거리 안에 있고
            {
                if (Vector3.Dot(Vector3.Normalize(monsters[i].transform.position - transform.position), transform.forward) > 0.3f) // 부채꼴의 공격범위에 들어왔을 때 공격한다.
                {
                    // 공격에 맞는 공격 효과와 이펙트 넣기
                    EffectManager.instance.OnEffect(CustomVariables.EFT_PLAYER_ATTACK, monsters[i].transform.position + monsters[i].transform.up, monsters[i].transform.forward);
                    monsters[i].TargetIsPlayer();
                    if (AttackTarget(monsters[i]))  // 몬스터가 죽었으면
                    {
                        monsterCount--;
                        i--;
                    }
                }
            }
        }
    }

    public void AttackMonster()     // 단일 공격
    {
        List<Monster> monsters = MonsterManager.instance.monsters;
        for (int i = 0; i < monsters.Count; ++i)
        {
            if (Vector3.SqrMagnitude(transform.position - monsters[i].transform.position) <
                (CustomVariables.RNG_PLAYER_BASE_ATTACK * CustomVariables.RNG_PLAYER_BASE_ATTACK))    // 적이 플레이어의 일정거리 안에 있고
            {
                if (Vector3.Dot(Vector3.Normalize(monsters[i].transform.position - transform.position), transform.forward) > 0.8f) // 적이 플레이어의 앞에 있다면
                {
                    // 공격에 맞는 공격 효과와 이펙트 넣기
                    EffectManager.instance.OnEffect(CustomVariables.EFT_PLAYER_ATTACK, monsters[i].transform.position + monsters[i].transform.up, monsters[i].transform.forward);
                    monsters[i].TargetIsPlayer();
                    AttackTarget(monsters[i]);
                    break;  // 한 마리만 공격한 뒤 break
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "FireBall")
        {
            EffectManager.instance.OnEffect(CustomVariables.EFT_MONSTER_ATTACK, collision.gameObject.transform.position + transform.up, transform.forward);
            GetComponent<DamageableObject>().TakeDamage(7);
            Destroy(collision.gameObject);
        }
    }

    public void GetHp(int amount)
    {
        hp += amount;
        if (hp > max_hp)
            hp = max_hp;
        UIManager.instance.SetPlayerStateText(hp, max_hp);
        EffectManager.instance.OnEffect(CustomVariables.EFT_HEAL_TEXT, transform.position, transform.forward, amount);
    }

    public void GetStemina(int amount)
    {
        stemina += amount;
        if (stemina > 100)
            stemina = 100;
    }
}
