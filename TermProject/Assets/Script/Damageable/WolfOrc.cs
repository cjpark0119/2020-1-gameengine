﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfOrc : Monster
{
    protected override void Awake()
    {
        base.Awake();
        max_hp = CustomVariables.HP_WOLF_ORC;
        hp = max_hp;
        damage = CustomVariables.DMG_WOLF_ORC;
        o_type = CustomVariables.WOLF_ORC;
        speed = CustomVariables.SPEED_WOLF_ORC;
        rotate_speed = 180f;
        attack_delay = 0;
        useAttackTime = CustomVariables.ATK_SPEED_SHAMAN_ORC;
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Attack()
    {
        if (cur_state == CustomVariables.STATE_MOVE)    // 처음 들어왔다면
        {
            useAttackTime = 0;
            attack_delay = 0;
            cur_state = CustomVariables.STATE_ATTACK;
            isAttack = false;
        }
        attack_delay += Time.deltaTime;

        if (isAttack == false && attack_delay > 0.1f)    // 0.5초 후에 공격
        {
            animator.SetTrigger("RunToAttack");

            EffectManager.instance.OnEffect(CustomVariables.EFT_MONSTER_ATTACK, transform.position + transform.up, transform.forward);
            if (isTargetGate)
                AttackTarget(gate.GetComponent<Gate>());
            else
                AttackTarget(player.GetComponent<Player>());
            isAttack = true;
        }
    }

    void Update()
    {
        if (GameManager.instance.running_game == true)
        {
            useAttackTime += Time.deltaTime;
            if (useAttackTime > 1 && cur_state == CustomVariables.STATE_ATTACK)
            {
                cur_state = CustomVariables.STATE_MOVE;
                animator.SetTrigger("AttackToRun");
            }

            Vector3 targetPos = isTargetGate ? g_transform.position : p_transform.position;

            if (cur_state == CustomVariables.STATE_ATTACK && attack_delay < 1)   // 현재 공격상태에 들어가 있다.
            {
                Attack();
            }
            else
            {
                if (Vector3.SqrMagnitude(targetPos - transform.position) < (CustomVariables.RNG_WOLF_ORC * CustomVariables.RNG_WOLF_ORC))  // 타겟이 시야에 있다면,
                {
                    if (useAttackTime > CustomVariables.ATK_SPEED_WOLF_ORC) // 시야에 있고 공격 쿨타임이 돌아왔다면
                    {
                        if (isTargetGate)
                            Attack();
                        else
                        {
                            if (Vector3.Dot(Vector3.Normalize(targetPos - transform.position), transform.forward) > 0.8f)
                                Attack();
                            else
                                MoveToTarget();
                        }
                    }
                }
                else
                    MoveToTarget();
            }
        }
    }
}
