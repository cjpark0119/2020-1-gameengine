﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 사용자 정의 변수들 작성

public class CustomVariables
{
    // effect 종류
    public const int EFT_PLAYER_ATTACK = 1;
    public const int EFT_MONSTER_ATTACK = 2;
    public const int EFT_GATE_SPLIT = 3;
    public const int EFT_DAMAGE_TEXT = 4;
    public const int EFT_HEAL_TEXT = 5;

    // 공격종류
    public const int ATK_PLAYER_BASE_ATTACK = 1;
    public const int ATK_PLAYER_POWER_ATTACK = 2;
    public const int ATK_WOLF_ORC_ATTACK = 3;
    public const int ATK_SHAMAN_ORC_ATTACK = 4;


    // 초기화에 사용되는 오브젝트 상태
    // -- hp
    public const int HP_PLAYER = 100;
    public const int HP_GATE = 500;
    public const int HP_WOLF_ORC = 40;
    public const int HP_SHAMAN_ORC = 30;

    // -- 공격범위
    public const int RNG_PLAYER_BASE_ATTACK = 4;
    public const int RNG_PLAYER_POWER_ATTACK = 9;
    public const int RNG_WOLF_ORC = 3;
    public const int RNG_SHAMAN_ORC = 15;

    // -- 데미지
    public const int DMG_PLAYER = 15;
    public const int DMG_WOLF_ORC = 10;
    public const int DMG_SHAMAN_ORC = 7;

    // -- 타입
    public const int PLAYER = 0;
    public const int WOLF_ORC = 1;
    public const int SHAMON_ORC = 2;
    public const int POSION_HP = 3;
    public const int POSION_STEMINA = 4;

    // -- 이동속도
    public const float SPEED_PLAYER_MAX = 9;
    public const float SPEED_PLAYER_BASE = 5;
    public const float SPEED_WOLF_ORC = 4;
    public const float SPEED_SHAMAN_ORC = 2.5f;

    public const float STEMINA_PLAYER = 100;

    // -- 공격 속도
    public const float ATK_SPEED_WOLF_ORC = 2.0f;
    public const float ATK_SPEED_SHAMAN_ORC = 3.5f;

    // -- 몬스터 현재 상태
    public const int STATE_MOVE = 1;
    public const int STATE_ATTACK = 2;


}
