﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShamanBall : MonoBehaviour
{
    public Vector3 dir;
    float lifeTime;
    float speed;
    float age;

    void Start()
    {
        age = 0.0f;
        lifeTime = 5.0f;
        speed = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        age += Time.deltaTime;
        transform.position = transform.position + dir * speed;

        if (age > lifeTime)
            Destroy(gameObject);
    }
}
