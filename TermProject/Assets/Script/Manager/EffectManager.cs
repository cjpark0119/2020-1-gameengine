﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour
{
    public static EffectManager instance = null;

    public GameObject playerAttackEffect;
    public GameObject monsterAttackEffect;
    public GameObject gateSplitEffect;
    public GameObject damageTextEffect;

    private Queue<ParticleSystem> effectList;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        effectList = new Queue<ParticleSystem>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (effectList.Count > 0)
        {
            while (true)
            {
                ParticleSystem particle = effectList.Peek();
                if (particle.isPlaying == false)
                {
                    effectList.Dequeue();
                    Destroy(particle.gameObject);
                    if (effectList.Count == 0) break;
                }
                else
                    break;
            }
        }
    }

    public void OnEffect(int type, Vector3 pos, Vector3 dir, int amount = 0)
    {
        switch (type)
        {
            case CustomVariables.EFT_PLAYER_ATTACK:
                effectList.Enqueue(Instantiate(playerAttackEffect, pos, Quaternion.identity).GetComponent<ParticleSystem>());
                break;
            case CustomVariables.EFT_MONSTER_ATTACK:
                effectList.Enqueue(Instantiate(monsterAttackEffect, pos + dir * 1.5f, Quaternion.identity).GetComponent<ParticleSystem>());
                break;
            case CustomVariables.EFT_GATE_SPLIT:
                effectList.Enqueue(Instantiate(gateSplitEffect, pos + dir * 1.5f, Quaternion.identity).GetComponent<ParticleSystem>());
                break;
            case CustomVariables.EFT_DAMAGE_TEXT:
                GameObject damageText = Instantiate(damageTextEffect, pos + dir * 1.5f, Quaternion.identity);
                damageText.GetComponent<DamageText>().SetDamageText(amount, false);
                break;
            case CustomVariables.EFT_HEAL_TEXT:
                GameObject healText = Instantiate(damageTextEffect, pos + dir * 1.5f, Quaternion.identity);
                healText.GetComponent<DamageText>().SetDamageText(amount, true);
                break;
        }
    }
}
