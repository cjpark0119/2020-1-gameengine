﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBarManager : MonoBehaviour
{
    public static HpBarManager instance = null;
    [SerializeField] GameObject hpBarPrefab;
    [SerializeField] GameObject gateHPBarPrefab;
    GameObject gatehpBar;
    List<GameObject> hpBarList;
    Transform camera_transform;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        hpBarList = new List<GameObject>();
    }

    void Start()
    {
        camera_transform = Camera.main.transform;
        gatehpBar = Instantiate(gateHPBarPrefab, new Vector3(0, 0, 0), Quaternion.identity, transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.running_game == true)
        {
            List<Monster> monsterList = MonsterManager.instance.monsters;
            Gate gate = BuildingManager.instance.gate;

            //hpBar 위치 조정 / 가시성 체크
            for (int i = 0; i < hpBarList.Count; i++)
            {
                if (Vector3.Dot(Vector3.Normalize(monsterList[i].transform.position - camera_transform.position), camera_transform.forward) > 0.1f)  // 플레이어의 시야에 보이는 몬스터만 Hp바를 띄우도록 함
                {
                    hpBarList[i].SetActive(true);
                    hpBarList[i].transform.position = Camera.main.WorldToScreenPoint(monsterList[i].transform.position + new Vector3(0, 2, 0));
                }
                else
                {
                    hpBarList[i].SetActive(false);
                }
            }

            if (Vector3.Dot(Vector3.Normalize(gate.transform.position - camera_transform.position), camera_transform.forward) > 0.1f)    // gate가 플레이어 시야에 보이면 Hp바를 띄우도록 함
            {
                gatehpBar.SetActive(true);
                gatehpBar.transform.position = Camera.main.WorldToScreenPoint(gate.transform.position + new Vector3(0, 5, 0));
            }
            else
            {
                gatehpBar.SetActive(false);
            }
        }
    }

    public void AddHpBar()
    {
        hpBarList.Add(Instantiate(hpBarPrefab, new Vector3(0, 0, 0), Quaternion.identity, transform));
    }

    public void UpdateGateHpBar()
    {
        Gate gate = BuildingManager.instance.gate;
        gatehpBar.GetComponent<HpBar>().SetBar(gate.hp, gate.max_hp);
        UIManager.instance.SetGateStateText(gate.hp, gate.max_hp);
    }

    public void UpdateHpBar(Monster monster)
    {
        List<Monster> monsterList = MonsterManager.instance.monsters;
        int monsterIdx = monsterList.FindIndex(item => item == monster);
        hpBarList[monsterIdx].GetComponent<HpBar>().SetBar(monster.hp, monster.max_hp);
    }

    public void DeleteHpBar(int idx = 0)
    {
        GameObject hpBar = hpBarList[idx];
        hpBarList.RemoveAt(idx);
        Destroy(hpBar);
    }

    public void DeleteAllHpBar()
    {
        int hpBarCount = hpBarList.Count;
        for (int i = 0; i < hpBarCount; ++i)
        {
            GameObject hpBar = hpBarList[0];
            hpBarList.RemoveAt(0);
            Destroy(hpBar);
        }    
    }
}
