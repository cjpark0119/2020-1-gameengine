﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    static public UIManager instance = null;
    private Transform stateFrame;
    private Transform scoreFrame;
    private Transform gameOverFrame;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        stateFrame = transform.Find("StateFrame");
        scoreFrame = transform.Find("ScoreFrame");
        gameOverFrame = transform.Find("GameOverFrame");
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public Transform GetGameOverFrame()
    {
        return gameOverFrame;
    }

    public void ClickToLobby()
    {
        SceneManager.LoadSceneAsync(0);
    }

    public void SetPlayerStateText(int curHP, int maxHP)
    {
        stateFrame.GetChild(2).GetComponent<Text>().text = curHP + "  /  " + maxHP; 
    }

    public void SetGateStateText(int curHP, int maxHP)
    {
        stateFrame.GetChild(3).GetComponent<Text>().text = curHP + "  /  " + maxHP;
    }

    public void SetScoreText(float score)
    {
        scoreFrame.GetChild(1).GetComponent<Text>().text = ((int)score).ToString();
    }
}
