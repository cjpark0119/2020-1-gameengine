﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager : MonoBehaviour
{
    public static MonsterManager instance = null;

    [SerializeField] private GameObject wolfOrcPrefab = null;
    [SerializeField] private GameObject shamanOrcPrefab = null;
    [SerializeField] private GameObject monsterSpawnerPrefab = null;
    [SerializeField] private GameObject posionSpawnerPrefab = null;
    [SerializeField] private GameObject hpPosionPrefab = null;
    [SerializeField] private GameObject steminaPosionPrefab = null;

    private float spawnTime;
    private List<GameObject> monsterSpawners;
    public List<Monster> monsters;
    private List<GameObject> posionSpawners;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        spawnTime = 0;
        monsters = new List<Monster>();
        monsterSpawners = new List<GameObject>();
        posionSpawners = new List<GameObject>();
    }

    void Start()
    {
        
    }

    
    void Update()
    {
        if (GameManager.instance.running_game == true)
        {
            spawnTime += Time.deltaTime;
            if (spawnTime > 10)      // 10초마다 몬스터 스포너가 생성된다.
            {
                spawnTime -= 10;
                Vector3 spawnerPos = new Vector3(Random.Range(-50f, 50f), 0, Random.Range(0f, 50f));
                GameObject monsterSpawner = Instantiate(monsterSpawnerPrefab, spawnerPos, Quaternion.Euler(-90f, 0f, 0f), transform);
                monsterSpawners.Add(monsterSpawner);
            }
        }
    }

    public void HatchMonster(GameObject spawner, int monsterType)
    {
        Vector3 spawnerPos = spawner.transform.position;
        monsterSpawners.Remove(spawner);
        Destroy(spawner);

        HpBarManager.instance.AddHpBar();
        if (monsterType == 0) {
            monsters.Add(Instantiate(wolfOrcPrefab, spawnerPos, Quaternion.identity, transform).GetComponent<Monster>());
        }
        else {
            monsters.Add(Instantiate(shamanOrcPrefab, spawnerPos, Quaternion.identity, transform).GetComponent<Monster>());
        }
    }

    public void DeleteMonster(Monster monster)
    {
        Vector3 pos = monster.transform.position;
        monsters.Remove(monster);
        Destroy(monster.gameObject);
        HpBarManager.instance.DeleteHpBar();        // 몬스터를 지우면서 HP바도 한 개 없엔다.

        if (Random.Range(0, 2) == 1)        // 몬스터가 죽으면 50%확률로 포션 아이템을 떨구는 포션 스포너를 생성한다.
        {
            GameObject posionSpawner = Instantiate(posionSpawnerPrefab, pos, Quaternion.Euler(-90f, 0f, 0f), transform);
            posionSpawners.Add(posionSpawner);
        }
    }

    public void HatchPosion(GameObject spawner, int posionType)
    {
        Vector3 spawnerPos = spawner.transform.position;
        posionSpawners.Remove(spawner);
        Destroy(spawner);

        if (posionType == 0)
        {
            Posion posion = Instantiate(hpPosionPrefab, spawnerPos + new Vector3(0,1,0), Quaternion.Euler(-90f, 0f, 0f), transform).GetComponent<Posion>();
            posion.posion_type = CustomVariables.POSION_HP;
        }
        else
        {
            Posion posion = Instantiate(steminaPosionPrefab, spawnerPos + new Vector3(0, 1, 0), Quaternion.Euler(-90f, 0f, 0f), transform).GetComponent<Posion>();
            posion.posion_type = CustomVariables.POSION_STEMINA;
        }
    }
}
