﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    public static BuildingManager instance = null;
    public GameObject GatePrefab;
    public GameObject wallPrefab;

    public Gate gate;
    private List<GameObject> buildings;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        gate = Instantiate(GatePrefab, new Vector3(0, 0, -65), Quaternion.identity, transform).GetComponent<Gate>();
    }

    void Start()
    {
        for(int i = 0; i < 7; ++i)
        {
            Instantiate(wallPrefab, new Vector3(16 + i *14, 5, -70), Quaternion.Euler(-90, 0, 0), transform);
        }
        for (int i = 0; i < 7; ++i)
        {
            Instantiate(wallPrefab, new Vector3(-16 - i * 14, 5, -70), Quaternion.Euler(-90, 0, 0), transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
