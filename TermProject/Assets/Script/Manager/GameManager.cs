﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public Player player;
    public MonsterManager monsterSpawner;
    public BuildingManager buildingManager;
    public EffectManager effectManager;
    public HpBarManager hpBarManager;
    private float score;
    public bool running_game;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        score = 0;
        running_game = true;
        Instantiate(buildingManager);
        Instantiate(monsterSpawner);
        Instantiate(effectManager);
        Instantiate(hpBarManager);
    }

    void Start()
    {
        
    }

    public int GetScore()
    {
        return (int)instance.score;
    }

    // Update is called once per frame
    void Update()
    {
        score += Time.deltaTime;
        UIManager.instance.SetScoreText(score);
    }
}
