﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource[] efxSource;
    public AudioSource[] musicSource;

    public AudioClip dieSound;
    public AudioClip hitSound;

    public static SoundManager instance = null;
    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(instance);

    }

    public void PlaySingle(AudioClip clip, float volume = 1.0f)
    {
        foreach (var src in efxSource)
        {
            if (src.isPlaying)
                continue;

            float randomPitch = Random.Range(lowPitchRange, highPitchRange);
            src.pitch = randomPitch;
            src.volume = volume;
            src.clip = clip;
            src.Play();
            break;
        }
    }
}
